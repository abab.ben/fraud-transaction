package com.demo.conferencedemo.repositories;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class IpStackClient implements IIpStackClient {
    // move to env variables
    private static String apiUrl = "http://api.ipstack.com/";
    // move to env variables
    private static String apiKey = System.getenv("apiKey");

    @Cacheable(cacheNames="ipsData", key="#ip")
    public String getInfoFromIpStack(String ip) throws Exception {
        try {
            //return "{\"ip\": \"134.201.250.155\", \"type\": \"ipv4\", \"continent_code\": \"NA\", \"continent_name\": \"North America\", \"country_code\": \"US\", \"country_name\": \"United States\", \"region_code\": \"CA\", \"region_name\": \"California\", \"city\": \"Los Angeles\", \"zip\": \"90012\", \"latitude\": 34.0655517578125, \"longitude\": -118.24053955078125, \"location\": {\"geoname_id\": 5368361, \"capital\": \"Washington D.C.\", \"languages\": [{\"code\": \"en\", \"name\": \"English\", \"native\": \"English\"}], \"country_flag\": \"https://assets.ipstack.com/flags/us.svg\", \"country_flag_emoji\": \"\\ud83c\\uddfa\\ud83c\\uddf8\", \"country_flag_emoji_unicode\": \"U+1F1FA U+1F1F8\", \"calling_code\": \"1\", \"is_eu\": false}}";
            String fullUrl = String.format("%s%s?access_key=%s", apiUrl, ip, apiKey);
            URL url = new URL(fullUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            String ipInformation = null;
            while ((output = br.readLine()) != null) {
                ipInformation = output;
            }
            conn.disconnect();
            if(ipInformation != null) {
                return ipInformation;
            }
            throw new Exception(String.format("Canno't parse response to string. Response content: %s", output));

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
            throw e;
        }
    }
}
