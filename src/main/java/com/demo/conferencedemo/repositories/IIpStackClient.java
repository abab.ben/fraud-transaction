package com.demo.conferencedemo.repositories;

public interface IIpStackClient {
    String getInfoFromIpStack(String ip) throws Exception;
}
