package com.demo.conferencedemo.controllers;

import com.demo.conferencedemo.logic.ICSVHelper;
import com.demo.conferencedemo.logic.IFraudTransactionLogic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/fraud/transaction")
public class FraudTransactionController {

    @Autowired
    private IFraudTransactionLogic fraudTransactionLogic;
    @Autowired
    private ICSVHelper csvHelper;


    @GetMapping
    public ResponseEntity<String> GetLastFile(){
        try {
            var response = csvHelper.GetLastFile();
            return new ResponseEntity<String>(response, HttpStatus.OK);
        }
        catch (Exception e) {
            // write log
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public HttpStatus UploadFile(@RequestParam("file") MultipartFile file){
        if (csvHelper.HasCSVFormat(file)) {
            try {
                fraudTransactionLogic.UploadFile(file);
                return HttpStatus.CREATED;
            } catch (Exception e) {
                // write log
                return HttpStatus.BAD_REQUEST;
            }
        }

        return HttpStatus.BAD_REQUEST;
    }
}
