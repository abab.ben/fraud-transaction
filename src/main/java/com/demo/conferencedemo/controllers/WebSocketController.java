//package com.demo.conferencedemo.controllers;
//
//import com.demo.conferencedemo.logic.IFraudTransactionLogic;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.stereotype.Controller;
//
//import javax.websocket.*;
//import javax.websocket.server.ServerEndpoint;
//
//@ServerEndpoint("/socket")
//@Controller
//public class WebSocketController {
//
//    @Autowired
//    private IFraudTransactionLogic fraudTransactionLogic;
//    @Autowired
//    private SimpMessagingTemplate template;
//
//    @OnOpen
//    public void open(Session session)
//    {
//        if(session != null)
//        {
//            System.out.println(session.getId() + " has opened a connection");
//            try
//            {
////              session.getBasicRemote().sendText("Connection Established");
//            }catch(Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    @OnClose
//    public void close(Session session)
//    {
//        if(session != null)
//        {
//            System.out.println(session.getId() + " has closed the session");
//            try
//            {
////              session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, "ended process"));
//            }catch(Exception e)
//            {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    @OnMessage
//    public String requestAccess(String message, Session session)
//    {
//        System.out.println("recieved message: " + message + " from user " + session.getId());
//        return "";
//    }
//
//    @OnError
//    public void onError(Throwable t)
//    {
//        new Exception(t).getStackTrace();
//    }
//
//}