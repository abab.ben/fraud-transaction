package com.demo.conferencedemo.logic;

import com.demo.conferencedemo.models.BasicCsv;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface ICSVHelper {
    boolean HasCSVFormat(MultipartFile file);

    List<BasicCsv> GetParsedFile(InputStream fileStream) throws IOException;

    void SaveNewCsvFile(String extendedCsvJsonList) throws Exception;

    String GetLastFile();
}
