package com.demo.conferencedemo.logic;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface IFraudTransactionLogic {
    void UploadFile(MultipartFile file) throws Exception;
}
