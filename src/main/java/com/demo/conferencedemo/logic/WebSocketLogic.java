package com.demo.conferencedemo.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Component
public class WebSocketLogic implements IWebSocketLogic {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public void SendSupiciousTransactionsMessage(String message) {
        this.simpMessagingTemplate.convertAndSend("/topic", message);
    }
}
