package com.demo.conferencedemo.logic;

public interface IWebSocketLogic {
    void SendSupiciousTransactionsMessage(String message);
}
