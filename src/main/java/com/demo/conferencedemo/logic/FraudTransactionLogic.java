package com.demo.conferencedemo.logic;

import com.demo.conferencedemo.models.BasicCsv;
import com.demo.conferencedemo.models.ExtendedCsv;
import com.demo.conferencedemo.models.IpStack.IpStackResponse;
import com.demo.conferencedemo.repositories.IIpStackClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class FraudTransactionLogic implements IFraudTransactionLogic {

    @Autowired
    private IIpStackClient ipStackClient;
    @Autowired
    private ICSVHelper csvHelper;
    @Autowired
    private IWebSocketLogic webSocketLogic;

    public void UploadFile(MultipartFile file) throws Exception {
        try {
            var parsedList = csvHelper.GetParsedFile(file.getInputStream());

            // send messages if there are suspicious users
            handleSuspectTransactions(parsedList);

            var extendedCsvList = new ArrayList<ExtendedCsv>();
            //var batchedLists = Lists.partition(parsedList, 50);
            for(BasicCsv basicCsv : parsedList){
                extendedCsvList.add(getInfoFromIpStack(basicCsv.ip, basicCsv));
            }

            var fullInfoTransactionList = ExtendedCsv.ConvertToJson(extendedCsvList);
            csvHelper.SaveNewCsvFile(fullInfoTransactionList);
        }
        catch(Exception ex){
            // write log
            throw ex;
        }
    }

    private void handleSuspectTransactions(List<BasicCsv> basicCsvs){
        var dictionaryHelper = new HashMap<String, BasicCsv>();
        var suspectUsers = new ArrayList<String>();

        for(BasicCsv basicCsv: basicCsvs){
            if(dictionaryHelper.containsKey(basicCsv.userId)){
                if(basicCsv.ip != dictionaryHelper.get(basicCsv.userId).ip) {
                    // write scary log
                    suspectUsers.add(basicCsv.userId);
                }
            }
            dictionaryHelper.put(basicCsv.userId, basicCsv);
        }

        if(suspectUsers.size() > 0){
            var suspectUsersString = String.join(",", suspectUsers);
            webSocketLogic.SendSupiciousTransactionsMessage(String.format("There are suspicious transactions, " +
                    "please check these user ids: %s", suspectUsersString));
        }
    }

    public ExtendedCsv getInfoFromIpStack(String ip, BasicCsv basicCsv) throws Exception {
        var ipStackInformation = ipStackClient.getInfoFromIpStack(basicCsv.ip);
        var response = IpStackResponse.ConvertStringToObject(ipStackInformation);
        var extendedCsv = new ExtendedCsv(basicCsv.id, basicCsv.userId, basicCsv.name, basicCsv.amount, basicCsv.ip, response);

        return extendedCsv;
    }
}
