package com.demo.conferencedemo.logic;

import com.demo.conferencedemo.models.BasicCsv;
import com.github.opendevl.JFlat;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CSVHelper implements ICSVHelper {
    public static final String CSV_TYPE = "text/csv";
    private static final String CSV_SEPARATOR = ",";
    private static final String LAST_FILE_KEY = "lastFileCsv";
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public boolean HasCSVFormat(MultipartFile file) {
        if (!CSV_TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    @Override
    public List<BasicCsv> GetParsedFile(InputStream fileStream) throws IOException {
        try {

            var parsedCsvList = new ArrayList<BasicCsv>();
            var fileReader = new BufferedReader(new InputStreamReader(fileStream, "UTF-8"));
            var csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT.withFirstRecordAsHeader());
            var csvRecords = csvParser.getRecords();
            for (CSVRecord csvRecord : csvRecords) {
                BasicCsv tutorial = new BasicCsv(
                        csvRecord.get("id"),
                        csvRecord.get("userId"),
                        csvRecord.get("name"),
                        Double.parseDouble(csvRecord.get("amount")),
                        csvRecord.get("ip")
                );
                parsedCsvList.add(tutorial);
            }

            return parsedCsvList;
        }
        catch (Exception ex){
            // write exception to log
            throw ex;
        }
    }

    public String GetLastFile(){
        try {
            var lastFile = redisTemplate.opsForValue().get(LAST_FILE_KEY);
            return lastFile;
        }
        catch (Exception ex){
            // write log
            throw ex;
        }
    }

    private void handelLastCsvCache(String extendedCsvJsonList){
        if(redisTemplate.opsForValue().get(LAST_FILE_KEY) != null){
            redisTemplate.delete(LAST_FILE_KEY);
        }
        redisTemplate.opsForValue().append(LAST_FILE_KEY, extendedCsvJsonList);
    }


    @Override
    @Transactional
    public void SaveNewCsvFile(String extendedCsvJsonList) throws Exception {
        try {
            var flatMe = new JFlat(extendedCsvJsonList);
            var a = flatMe.json2Sheet();
            var fileName = String.format("%s.csv", System.currentTimeMillis());
            flatMe.write2csv(fileName, ',');

            handelLastCsvCache(extendedCsvJsonList);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
