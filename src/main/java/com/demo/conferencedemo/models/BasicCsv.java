package com.demo.conferencedemo.models;

public class BasicCsv {
    public String id;
    public String userId;
    public String name;
    public Double amount;
    public String ip;

    public BasicCsv(String id, String userId, String name, Double amount, String ip){
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.amount = amount;
        this.ip = ip;
    }
}
