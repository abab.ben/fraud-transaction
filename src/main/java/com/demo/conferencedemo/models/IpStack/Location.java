package com.demo.conferencedemo.models.IpStack;

import java.util.ArrayList;

public class Location {
    public int geoname_id;
    public String capital;
    public ArrayList<Language> languages;
    public String country_flag;
    public String country_flag_emoji;
    public String country_flag_emoji_unicode;
    public String calling_code;
    public boolean is_eu;
}
