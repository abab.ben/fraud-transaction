package com.demo.conferencedemo.models.IpStack;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IpStackResponse {
        public String ip;
        public String hostname;
        public String type;
        public String continent_code;
        public String continent_name;
        public String country_code;
        public String country_name;
        public String region_code;
        public String region_name;
        public String city;
        public String zip;
        public double latitude;
        public double longitude;
        public Location location;
        public TimeZone time_zone;
        public Currency currency;
        public Connection connection;
        public Security security;

        public static IpStackResponse ConvertStringToObject(String ipStackResponse) throws JsonProcessingException {
                ObjectMapper mapper = new ObjectMapper();
                try{
                        var parseIpStackResponse= mapper.readValue(ipStackResponse, IpStackResponse.class);
                        return parseIpStackResponse;
                }
                catch (Exception e) {
                        e.printStackTrace();
                        throw e;
                }
        }
}
