package com.demo.conferencedemo.models.IpStack;

public class Security {
    public boolean is_proxy;
    public Object proxy_type;
    public boolean is_crawler;
    public Object crawler_name;
    public Object crawler_type;
    public boolean is_tor;
    public String threat_level;
    public Object threat_types;
}
