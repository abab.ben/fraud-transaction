package com.demo.conferencedemo.models.IpStack;

import java.util.Date;

public class TimeZone {
    public String id;
    public Date current_time;
    public int gmt_offset;
    public String code;
    public boolean is_daylight_saving;
}
