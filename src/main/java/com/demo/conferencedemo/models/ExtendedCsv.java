package com.demo.conferencedemo.models;

import com.demo.conferencedemo.models.IpStack.IpStackResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class ExtendedCsv extends BasicCsv{
    public IpStackResponse ipStackResponse;

    public ExtendedCsv(String id, String userId, String name, Double amount, String ip, IpStackResponse ipStackResponse){
        super(id, userId, name, amount, ip);
        this.ipStackResponse = ipStackResponse;
    }

    public static String ConvertToJson(List<ExtendedCsv> extendedCsvList) throws JsonProcessingException {
        try {
            var objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
            return objectWriter.writeValueAsString(extendedCsvList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw e;
        }
    }

}
